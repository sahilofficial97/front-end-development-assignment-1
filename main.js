
let balance=200;
let pay=0;
let loan=0;
let salary=0;
let laptops=[];


fetch("https://hickory-quilled-actress.glitch.me/computers")
    .then(response=>response.json())
    .then(data=>laptops=data)
    .then(drinks=>addLaptopsToMenu(drinks));


//display balance in HTML
document.getElementById("balance-amount").innerHTML=amountInEuro(balance);

//display salary amount 
document.getElementById("work-amount").innerHTML=amountInEuro(salary);

//When get loan is pressed
document.getElementById("btn-loan").addEventListener("click", subtmitEvent);

//hide pay loan button
document.getElementById("pay-loan").style.visibility="hidden";

//when pay loan button is pressed
document.getElementById("pay-loan").addEventListener("click",()=>{
    if(salary>loan){
        console.log(loan)
        salary=salary-loan
        console.log(salary)
        document.getElementById("pay-loan").style.visibility="hidden";
    }else{
        loan=loan-salary;
    }
    transferToBank()
})

//when bank button is pressed
document.getElementById("bank-transfer").addEventListener("click", transferToBank);

//when work button is pressed
document.getElementById("work").addEventListener("click", ()=>{
    let work=100;
    salary=salary+work
    console.log(salary);
    document.getElementById("work-amount").innerHTML=amountInEuro(salary);
});

//when selected laptop change
document.querySelector("#laptop-list").addEventListener("change",handlLaptopMenuChange)



//when buy btn is pressed
function buybtn(price){
    document.getElementById("btn-buy").addEventListener("click",(e)=>{
        if(balance>price){
            balance=balance-price
            document.getElementById("balance-amount").innerHTML=amountInEuro(balance);document.querySelector(".error-message").insertAdjacentHTML('afterbegin', allertTemp("You know own the hardware"));
            setTimeout(() => {
                document.getElementById("message").remove();
            }, "3000")
        }else{
            document.getElementById("balance-amount").innerHTML=amountInEuro(balance);document.querySelector(".error-message").insertAdjacentHTML('afterbegin', allertTemp("You dont have enough balance for this hardware"));
            setTimeout(() => {
                document.getElementById("message").remove();
            }, "3000")
        }
    })
}

// transfer money to bank
function transferToBank(){
    let amountDeduct=0;
    const checkLoan=loan/100*75;
    console.log(checkLoan);
    if(loan>checkLoan){
        amountDeduct=salary/100*10
    }

    salary=salary-amountDeduct;
    balance=balance+salary;
    salary=0;
    document.getElementById("work-amount").innerHTML=amountInEuro(salary);
    document.getElementById("balance-amount").innerHTML=amountInEuro(balance);
}

//event when get loan
function subtmitEvent(){
    document.getElementById("loan-submit").addEventListener("click", ()=>{
        bootstrap.Modal.getOrCreateInstance('#exampleModal').hide();
        let loanAmount=document.getElementById("loan-value").value;
        checkLoan(loanAmount);
    })
};

//this functin wil check if the amount enter for loan is correct
function checkLoan(amount){
    if(balance<amount){
        document.querySelector(".error-message").insertAdjacentHTML('afterbegin', allertTemp("Error creating loan bigger than your balance"));
        setTimeout(() => {
            document.getElementById("message").remove();
        }, "3000")
        return;
    }

    if(loan>0){
        document.querySelector(".error-message").insertAdjacentHTML('afterbegin', allertTemp("You already have a loan"));
        setTimeout(() => {
            document.getElementById("message").remove();
        }, "3000")
        return;
    }

    loan=amount;
    balance=balance+parseInt(loan);
    document.getElementById("balance-amount").innerHTML=amountInEuro(balance);
    document.getElementById("pay-loan").style.visibility="visible";
}

//fuction return amount in euro
function amountInEuro(number){
    return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(number)
};

//Allert template
function allertTemp(message){
    return `<div class="alert alert-info" role="alert" id="message">${message}</div>`
}

//
function addLaptopsToMenu(drinks){
    drinks.forEach(d => laptopOptionTemp(d));
}

//Allert template
function laptopOptionTemp(drink){
    const temp=`<option value="${drink.id}">${drink.title}</option>`
    document.querySelector("#laptop-list").insertAdjacentHTML('afterbegin', temp);
}

//
function handlLaptopMenuChange(e){

    const selectedLaptop=laptops[e.target.selectedIndex]
    const imgsrc=selectedLaptop.image.replace("assets/images","https://hickory-quilled-actress.glitch.me/assets/images");
    document.querySelector("#laptop-feature").innerText=selectedLaptop.specs
    document.querySelector("#laptop-title").innerText=selectedLaptop.title
    document.querySelector("#laptop-desc").innerText=selectedLaptop.description
    document.querySelector("#laptop-price").innerText=amountInEuro(selectedLaptop.price)
    document.querySelector("#laptop-img").src=imgsrc
    buybtn(selectedLaptop.price)
}
